#ifndef Ranking_ED1_h
#define Ranking_ED1_h
#include "Arduino.h"

class Ranking_ED1{
	public:
		Ranking_ED1(int pin);//Aqui deve receber o ranking do arquivo);
		void iniciarSerial(HardwareSerial &print);
		bool inserirNoRanking(int valor);
		bool exibirRanking();
		bool removerElemento(int valor);
		bool buscarElemento(int valor);
		
	private:
			int _lista;
			HardwareSerial* printx;

};

#endif
