#include "Arduino.h"
#include "Ranking_ED1.h"
#include <stdbool.h>
#define TAM_RANKING 3
int tamanho = 0;
int ranking[TAM_RANKING];


Ranking_ED1::Ranking_ED1(int pin){
	pinMode(pin,OUTPUT);
	_lista = pin;
	}
	
	void Ranking_ED1::iniciarSerial(HardwareSerial &print){
	 printx = &print; //operate on the address of print
     printx->begin(9600);
     printx->println("Iniciando Serial");
     for(int i = 0; i < 10; i++){
     	printx->print(".");
	 	delay(500);
    }
	 printx->println("\n");
}
	
bool Ranking_ED1::removerElemento(int valor){
		printx->println("Entrando funcao remover");
		int i,j,cont=0;

	if(tamanho==0) {//verifica se a lista esta vazia
		printx->println("Ranking vazio!");
		return false;
	}

	for(i=0; i<tamanho && valor>=ranking[i] ;i++) {
		if(valor==ranking[i]) {
			for(j=i;j<tamanho-1;j++) {
				ranking[j]=ranking[j+1];//faz os elementos darem um passo a frente
			}
			tamanho--;
			i--;//faz o i voltar para o caso de haver numero repetidos
			cont++;
		}
	}
	if(cont!=0)
		printx->println("Removendo elemento.");
	return true;
	else
		printx->println("Valor nao esta na lista");
	return false;
}

bool Ranking_ED1::inserirNoRanking(int valor){
	int i;
	if(tamanho==TAM_RANKING-1){ //Verifica se lista esta cheia
	printx->println("#########################################");
	printx->println("#SIMON, THE GAME - ESTRUTURA DE DADOS 1 #");
	printx->println("#########################################");
	printx->println("#                                       #");
	printx->println("#             Lista cheia!              #");
	printx->println("#                                       #");
	printx->println("#########################################");
	return false;
	} else{
	
	for (i = tamanho; i > 0 && valor < ranking[i-1]; i--){
		ranking[i]=ranking[i-1];
	}
	ranking[i]=valor;
	tamanho++;
	printx->println("#########################################");
	printx->println("#SIMON, THE GAME - ESTRUTURA DE DADOS 1 #");
	printx->println("#########################################");
	printx->println("#                                       #");
	printx->println("#    Elemento inserido com sucesso!     #");
	printx->println("#                                       #");
	printx->println("#########################################");
	return true;
	}
}

bool Ranking_ED1::exibirRanking(){
	int u;
	if (tamanho == 0){
	printx->println("#########################################");
	printx->println("#            Ranking Vazio!             #");
	printx->println("#########################################");
	return false;
	} else{	
	printx->println("#########################################");
	printx->println("#           Mostrando ranking!          #");
	printx->println("#                                       #");
		for (u = 0; u <= tamanho-1;u++){
		printx->print("#    ");
		printx->print(u+1);
		//printx->print(" - ");
		printx->print("# ");
		printx->print(ranking[u]);
		printx->println("\t\t\t\t#");
		}
	printx->println("#                                       #");
	return true;
}	
}

bool Ranking_ED1::buscarElemento(int valor){
	int i,aux=0;
	if(tamanho==0) {
		printx->println("Ranking vazio!");
		return false ;
	}

	for(i=0;i<tamanho && valor>=ranking[i];i++) {
		if(valor==ranking[i]) {
			printx->print("O valor: ");
			printx->println(valor);
			printx->print(" Esta em: ");
			printx->println(i+1);
			aux++;
			return true;
		}
	}
	if(aux==0)
		printx->println("Valor nao esta na lista!");
}