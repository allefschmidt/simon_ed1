/*
Descricao
*/

#ifndef Simon_h
#define Simon_h

#include "Arduino.h"

class Simon{
	public:
		Simon(int pin);
		Simon(int pin,int pin2,int pin3,int pin4,int but,int but2,int but3,int but4, int som);
		void iniciarSerial(HardwareSerial &print);
		void testarLedSom();
		void iniciarMusica();
		void gerarAleatorio();
		void pegarBotao();
		void fail();
		void menu();
	
	private:
		int _som;
		int _pinTeste;
		int _pinLedVermelho;
		int _pinLedAmarelo;
		int _pinLedAzul;
		int _pinLedVerde;
		int _buttonVermelho;
		int _buttonAmarelo;
		int _buttonAzul;
		int _buttonVerde;
		HardwareSerial* printer;
};	

#endif
