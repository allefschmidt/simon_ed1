#include "Arduino.h"
#include "HardwareSerial.h"
#include "Simon.h"
#include "Tone.h"
#define LED_VERMELHO 13
#define LED_AMARELO 11
#define LED_VERDE 10

Tone speakerpin;
int starttune[] = {NOTE_C4, NOTE_F4, NOTE_C4, NOTE_F4, NOTE_C4, NOTE_F4, NOTE_C4, NOTE_F4, NOTE_G4, NOTE_F4, NOTE_E4, NOTE_F4, NOTE_G4};
int duration2[] = {100, 200, 100, 200, 100, 400, 100, 100, 100, 100, 200, 100, 500};
int note[] = {NOTE_C4, NOTE_C4, NOTE_G4, NOTE_C5, NOTE_G4, NOTE_C5};
int duration[] = {100, 100, 100, 300, 100, 300};

int turn = 0;  // turn counter
int buttonstate = 0;  // button state checker
int randomArray[100]; //Intentionally long to store up to 100 inputs (doubtful anyone will get this far)
int inputArray[100];  

Simon::Simon(int pin){
	pinMode(pin, OUTPUT);
	_pinTeste = pin;
}
Simon::Simon(int pin,int pin2,int pin3,int pin4,int but,int but2,int but3,int but4, int som){
			
	pinMode(pin,OUTPUT);
	pinMode(pin2,OUTPUT);
	pinMode(pin3,OUTPUT);
	pinMode(pin4,OUTPUT);
	pinMode(but,INPUT);
	pinMode(but2,INPUT);
	pinMode(but3,INPUT);
	pinMode(but4,INPUT);
	speakerpin.begin(som);
	_pinLedVermelho = pin;
	_pinLedAmarelo = pin2;
	_pinLedAzul = pin3;
	_pinLedVerde = pin4;
	_buttonVermelho = but;
	_buttonAmarelo = but2;
	_buttonAzul = but3;
	_buttonVerde = but4;
	_som = som;
	randomSeed(analogRead(0));

	}
void Simon::iniciarSerial(HardwareSerial &print){
	 printer = &print; //operate on the address of print
     printer->begin(9600);
     printer->println("Iniciando Serial");
     for(int i = 0; i < 10; i++){
     	printer->print(".");
	 	delay(500);
    }
	 printer->println("\n");
}
void Simon::testarLedSom(){
	digitalWrite(_pinLedVermelho,HIGH);
	digitalWrite(_pinLedAmarelo,HIGH);
	digitalWrite(_pinLedAzul,HIGH);
	digitalWrite(_pinLedVerde,HIGH);
  	printer->println("Leds Ligados");
    
	for (int thisNote = 0; thisNote < 6; thisNote ++) {
     // play the next note:
     speakerpin.play(note[thisNote]);
     // hold the note:
     delay(duration[thisNote]);
     // stop for the next note:
     speakerpin.stop();
     delay(25);
    }
    
 	digitalWrite(_pinLedVermelho,LOW);
	digitalWrite(_pinLedAmarelo,LOW);
	digitalWrite(_pinLedAzul,LOW);		
	digitalWrite(_pinLedVerde,LOW);	
	printer->println("Leds Desligados");
}
void Simon::iniciarMusica(){
for (int thisNote = 0; thisNote < 13; thisNote ++) {
     // play the next note:
     speakerpin.play(starttune[thisNote]);
     // hold the note:
     if (thisNote==0 || thisNote==2 || thisNote==4 || thisNote== 6)
     {
       digitalWrite(_pinLedVermelho, HIGH);
     }
     if (thisNote==1 || thisNote==3 || thisNote==5 || thisNote== 7 || thisNote==9 || thisNote==11)
     {
       digitalWrite(_pinLedAmarelo, HIGH);
     }
     if (thisNote==8 || thisNote==12)
     {
       digitalWrite(_pinLedAzul, HIGH);
     }  
     if (thisNote==10)
     {   
       digitalWrite(_pinLedVerde, HIGH);
     }
     delay(duration2[thisNote]);
     // stop for the next note:
     speakerpin.stop();
     digitalWrite(_pinLedVermelho, LOW);
     digitalWrite(_pinLedAmarelo, LOW);
     digitalWrite(_pinLedAzul, LOW);
     digitalWrite(_pinLedVerde, LOW);
     delay(25);
    }
}
void Simon::gerarAleatorio(){
	
	int ledpin[] = {_pinLedVermelho, _pinLedAzul, _pinLedVerde, _pinLedAmarelo};	
    for (int y=turn; y <= turn; y++)
    { //Limited by the turn variable
      Serial.println(""); //Some serial output to follow along
      printer->println("");
      Serial.print("Turn: ");
      Serial.print(y);
      Serial.println("");
      randomArray[y] = random(1, 5); //Assigning a random number (1-4) to the randomArray[y], y being the turn count
     
	  for (int x=0; x <= turn; x++)
      {
        Serial.print(randomArray[x]);
      
        for(int y=0; y<4; y++)
        {
      
          if (randomArray[x] == 1 && ledpin[y] == _pinLedVermelho) 
          {  //if statements to display the stored values in the array
            digitalWrite(ledpin[y], HIGH);
            speakerpin.play(NOTE_G3, 100);
            delay(400);
            digitalWrite(ledpin[y], LOW);
            delay(100);
          }

          if (randomArray[x] == 2 && ledpin[y] == _pinLedAzul) 
          {
            digitalWrite(ledpin[y], HIGH);
            speakerpin.play(NOTE_A3, 100);
            delay(400);
            digitalWrite(ledpin[y], LOW);
            delay(100);
          }
  
          if (randomArray[x] == 3 && ledpin[y] == _pinLedVerde) 
          {
            digitalWrite(ledpin[y], HIGH);
            speakerpin.play(NOTE_B3, 100);
            delay(400);
            digitalWrite(ledpin[y], LOW);
            delay(100);
          }

          if (randomArray[x] == 4 && ledpin[y] == _pinLedAmarelo) 
          {
            digitalWrite(ledpin[y], HIGH);
            speakerpin.play(NOTE_C4, 100);
            delay(400);
            digitalWrite(ledpin[y], LOW);
            delay(100);
          }
        }
      }
    }
    pegarBotao();
}
void Simon::pegarBotao(){
	int ledpin[] = {_pinLedVermelho, _pinLedAzul, _pinLedVerde, _pinLedAmarelo};
	int button[] = {_buttonVermelho,_buttonAmarelo,_buttonAzul,_buttonVerde};	
	int x;
	for (x = 0; x <= turn;) { //Statement controlled by turn count

    for(int y=0; y<4; y++)
    {
      
      buttonstate = digitalRead(button[y]);
    
      if (buttonstate == LOW && button[y] == 2)
      { //Checking for button push
        digitalWrite(ledpin[0], HIGH);
        speakerpin.play(NOTE_G3, 100);
        delay(200);
        digitalWrite(ledpin[0], LOW);
        inputArray[x] = 1;
        delay(250);
        Serial.print(" ");
        Serial.print(1);
        if (inputArray[x] != randomArray[x]) { //Checks value input by user and checks it against
          fail();                              //the value in the same spot on the generated array
        }                                      //The fail function is called if it does not match
        x++;
      }
       if (buttonstate == LOW && button[y] == 3)
      {
        digitalWrite(ledpin[1], HIGH);
        speakerpin.play(NOTE_A3, 100);
        delay(200);
        digitalWrite(ledpin[1], LOW);
        inputArray[x] = 2;
        delay(250);
        Serial.print(" ");
        Serial.print(2);
        if (inputArray[x] != randomArray[x]) {
          fail();
        }
        x++;
      }

      if (buttonstate == LOW && button[y] == 4)
      {
        digitalWrite(ledpin[2], HIGH);
        speakerpin.play(NOTE_B3, 100);
        delay(200);
        digitalWrite(ledpin[2], LOW);
        inputArray[x] = 3;
        delay(250);
        Serial.print(" ");
        Serial.print(3);
        if (inputArray[x] != randomArray[x]) {
          fail();
        }
        x++;
      }

      if (buttonstate == LOW && button[y] == 5)
      {
        digitalWrite(ledpin[3], HIGH);
        speakerpin.play(NOTE_C4, 100);
        delay(200);
        digitalWrite(ledpin[3], LOW);
        inputArray[x] = 4;
        delay(250);
        Serial.print(" ");
        Serial.print(4);
        if (inputArray[x] != randomArray[x]) 
        {
          fail();
        }
        x++;
      }
    }
  }
  delay(500);
  turn++; //Increments the turn count, also the last action before starting the output function over again
}
void Simon::fail(){
	int i;
	for (int y=0; y<=2; y++)
  	{ //Flashes lights for failure
    
	int ledpin[] = {_pinLedVermelho, _pinLedAzul, _pinLedVerde, _pinLedAmarelo};
    for (i = 0; i < 4; i++){
    	digitalWrite(ledpin[i],HIGH);
	}
	speakerpin.play(NOTE_G3, 300);
    delay(200);
    for (i = 0; i < 4; i++){
    	digitalWrite(ledpin[i],LOW);
	}
    speakerpin.play(NOTE_C3, 300);
    delay(200);
  }
  delay(500);
  turn = -1; //Resets turn value so the game starts over without need for a reset button
}

void Simon::menu(){
	printer->println("########################################");
	printer->println("#SIMON, THE GAME - ESTRUTURA DE DADOS 1#");
	printer->println("########################################");
	printer->println("#   Digite item no menu abaixo         #");
	printer->println("#                                      #");
	printer->println("#   1. Iniciar Jogo                    #");;
	printer->println("#   2. Buscar elemento                 #");
	printer->println("#   3. Exibir Ranking                  #");
	printer->println("#   4. Sair                            #");
	printer->println("#                                      #");
	printer->println("########################################");
}