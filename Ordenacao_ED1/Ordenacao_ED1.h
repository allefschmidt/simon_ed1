#ifndef Ordenacao_ED1_h
#define Ordenacao_ED1_h

#include "Arduino.h"
class Ordenacao_ED1{
	public:
		Ordenacao_ED1(int pin);
		void bubble_sort(float A[], int len);
		void quick_sort();
		void merge_sort();
	private:
		int _pinoCSO;
};


#endif
